package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    public String attack() {
        return "Attack with magic! Elohim, esaim, I implore you!";
    }

    public String getType() {
        return "Magic";
    }
}
