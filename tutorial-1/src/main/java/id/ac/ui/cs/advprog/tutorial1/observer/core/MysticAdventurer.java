package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
        guild.add(this);
    }

    @Override
    public void update() {
        String questType = this.guild.getQuestType();
        if (questType.equals("D") || questType.equals("E")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
