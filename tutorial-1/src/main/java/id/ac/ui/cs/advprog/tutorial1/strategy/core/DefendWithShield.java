package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    public String defend() {
        return "Defend with barrier! Aegis Shield activated!";
    }

    public String getType() {
        return "Shield";
    }
}
