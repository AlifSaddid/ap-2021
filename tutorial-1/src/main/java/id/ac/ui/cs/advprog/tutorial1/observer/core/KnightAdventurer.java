package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
        guild.add(this);
    }

    public void update() {
        Quest quest = this.guild.getQuest();
        this.getQuests().add(quest);
    }
}
